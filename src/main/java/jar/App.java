package jar;
import java.util.Properties;
import org.python.core.*;
import org.python.util.*;

public class App { 
    public static void main(String[] args) throws PyException { 
        PythonInterpreter interp = new PythonInterpreter();
        Properties python = new Properties();
        python.setProperty("python.home", "/usr/lib/python3.5");
        python.setProperty("python.path", "/usr/lib/python3.5");
        PythonInterpreter.initialize(System.getProperties(), python, new String[0]);
        interp.exec("print('Hello world!')");
    }
}
