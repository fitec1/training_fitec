

import tkinter as tk
from tkinter import messagebox 
from functools import partial
from pymongo import MongoClient, ASCENDING

yellow = "#ECF30E"
green = "#2BB42B"
grey = "#99A7BC"
blue = "#1767DF"
red = "#EB2511"
lightred = "#F0AFA3"
lightyellow = '#F0F3C1'

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        # master
        self.master = master
        self.create_widgets()
        self.pack()
        # database
        client = MongoClient('mongodb://localhost:27017/')
        db = client.DEVOPS_PHONE
        self.users = db.users
        ## unique name and unique phone
        self.users.create_index([('phone', ASCENDING)], unique=True)
        self.users.create_index([('name', ASCENDING)], unique=True)

    def create_widgets(self):
        # search button
        self.searchButton = tk.Button(self, activebackground=grey, width= 15, font = "Helvetica 16 bold")
        self.searchButton["text"] = "Search contact"
        self.searchButton["command"] = self.search
        self.searchButton.grid(row = 3, column = 6, sticky = "E", pady = 2, padx = 20)
        # save button
        self.saveButton = tk.Button(self,activebackground=grey, width= 15, font = "Helvetica 16 bold")
        self.saveButton["text"] = "Save contact"
        self.saveButton["command"] = self.save
        self.saveButton.grid(row = 6, column = 6, sticky = "E", pady = 2, padx = 20)        
        # clear button
        self.clearButton = tk.Button(self, activebackground=red, background=lightred, width= 15, font = "Helvetica 16 bold")
        self.clearButton["text"] = "Clear"
        self.clearButton["command"] = self.clear
        self.clearButton.grid(row = 9, column = 6, sticky = "E", pady = 2, padx = 20)
        
        # entry name
        self.entryContact = tk.Entry(self, bd = 2, font = "Helvetica 16 bold")
        self.entryContact.grid(row = 1, column = 0, sticky = "W", pady = 2, columnspan= 3)
        # label name
        self.labelName = tk.Label(self, text = "* Name (>3)", fg= blue)
        self.labelName.grid(row = 0, column = 0, sticky = "W", pady = 2, columnspan= 3)
        
        # entry phone
        self.entryPhone = tk.Entry(self, bd = 2, font = "Helvetica 16 bold", fg=red, state='disabled')
        self.entryPhone.grid(row = 4, column = 0, sticky = "W", pady = 2, columnspan= 3)
        # label phone
        self.labelPhone = tk.Label(self, text = "* Phone (10)", fg= blue)
        self.labelPhone.grid(row = 3, column = 0, sticky = "W", pady = 2, columnspan= 3)

        for i in range(1,10):
            temp = tk.Button(self, activebackground=yellow, background=lightyellow, width= 2, font="Helvetica 12 bold")
            temp["text"] = str(i)
            temp["command"] = partial(self.writeNumber, i)
            temp.grid(row = 10+ int((i -1)/3), column = (i-1)%3 , sticky = "E", pady = 2, padx = 2)
            self.__setattr__("button"+str(i), temp)
        
        # zeo button
        self.button0 = tk.Button(self, activebackground=yellow, background=lightyellow, width= 2, font="Helvetica 12 bold")
        self.button0["text"] = "0"
        self.button0["command"] = partial(self.writeNumber, 0)
        self.button0.grid(row = 13, column = 1 , sticky = "E", pady = 2, padx = 2)

        # list contact 
        self.contactList = tk.Listbox(self, width= 35)
        tk.Scrollbar(self.contactList, orient="vertical")
        self.contactList.grid(row = 15, column = 6 , sticky = "W", pady = 2, padx = 4 , columnspan= 4)

    def writeNumber(self, i):
        self.entryPhone.configure(state="normal")
        index = len(self.entryPhone.get())
        if index < 10: self.entryPhone.insert(index, str(i))
        self.entryPhone.configure(state="disabled")

    def search(self):
        try:
            st ='.*'+self.entryContact.get()+'.*'
            users = self.users.find({"name": { 
                '$regex' :st
                }
            })
            i=0
            self.contactList.delete(0, 'end')
            for user in users:
                i+=1
                ss = ''
                for k in user:
                    if k != "_id":
                        if ss == '': ss=k + " : " +user[k] 
                        else: ss = ss + " -- " + k + " : " +user[k]                  
                self.contactList.insert(i, ss)        
                        
        except Exception as err:
            print(err)
            tk.messagebox.showerror("Mongo Error Message","Error: Some thin went wrong, no contact found!")
    def save(self):
        if len(self.entryContact.get()) < 3:
            tk.messagebox.showerror("Python Error Message","Error: Bad name, must have at least 3 carracters!")
        elif len(self.entryPhone.get()) != 10:
            tk.messagebox.showerror("Python Error Message","Error: Bad phone, must have 10 numbers!")
        else:
            try:
                res = self.users.insert_one({
                    "name": self.entryContact.get(),
                    "phone": self.entryPhone.get()
                })
                if res: tk.messagebox.showinfo("Mongo Info Message","Congratulations: " + self.entryContact.get() + " contact correctly saved!")
            except Exception as err:
                print(err)
                if err.code == 11000 and "phone" in str(err.details):
                    tk.messagebox.showerror("Mongo Error Message","Error: dupplicated phone, " + self.entryPhone.get() + " already exist!")
                elif err.code == 11000 and "name" in str(err.details):
                    tk.messagebox.showerror("Mongo Error Message","Error: dupplicated name, " + self.entryContact.get() + " already exist!")
                else:  
                    tk.messagebox.showerror("Mongo Error Message","Error: Some thin went wrong, contact not saved!")

    def clear(self):
        self.entryContact.delete(0, 'end')
        self.entryPhone.configure(state="normal")
        self.entryPhone.delete(0, 'end')
        self.entryPhone.configure(state="disabled")  

# root
root = tk.Tk()
root.geometry("700x700+200+50")

# application
app = Application(master=root)
app.master.title('Phonebook')
app.mainloop()