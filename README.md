# Description

This is a widget application made by Python. This widget allows to save contacts in a mongo database and retreive and export them to a file.

# Requirements for python

tkinter, functools, pymongo

# Use

For linux users, enable mongod by,

```
sudo systemuctl start mongod
```

To use with maven,

```
mvn complie
```
or,

```
mvn install
```

it can also be run without maven by executing the pyhton file `` application.py`` in ``src/main/ressources``
